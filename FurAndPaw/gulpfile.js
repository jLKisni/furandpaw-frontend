var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-ruby-sass'),
	connect = require('gulp-connect'),
	plumber = require('gulp-plumber'),
	imagemin = require('gulp-imagemin'),
	prefix = require('gulp-autoprefixer');


//script task
//Uglifies
gulp.task('scripts',function(){
	gulp.src('js/*.js')
	.pipe(plumber())
	.pipe(uglify())
	.pipe(gulp.dest('build/js'))
	.pipe(connect.reload());
});

//watch task
//js,sass,livereload
gulp.task('watch',function(){
	gulp.watch('js/*.js',['scripts']);
	gulp.watch('scss/**/*.scss',['styles']);
	gulp.watch('admin/*.html',['serve']);
});


gulp.task('serve',function(){

	gulp.src('admin/*.html')
		.pipe(connect.reload());
});

//styles
//sass
gulp.task('styles', function() {

 return sass('scss/**/*.scss', {
  style: 'expanded'
 })
  .pipe(plumber())
  .pipe(prefix('last 2 versions'))
  .pipe(gulp.dest('css/'))
  .pipe(connect.reload());
});﻿

//image task
gulp.task('image', function() {

	gulp.src(['img/**/*','img/*','img/**/**/*'])
	.pipe(imagemin())
	.pipe(gulp.dest('img'));
});﻿

gulp.task('connect', function() {
    connect.server({
        livereload: true
    });
});


gulp.task('default',['connect','scripts','watch','styles','image','serve']);