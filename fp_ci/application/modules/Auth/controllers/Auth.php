<?php
  
class Auth extends MY_Controller{

    function __construct(){
      parent::__construct();
      $this->load->model('Auth/M_Auth');
      $this->load->library('form_validation');


    }

    function signup(){
        $data = $this->input->post('data');

        $userdetails = array(
          'email'=> $data['email'],
          'username' =>$data['username'],
          'password'=> md5($data['password']),
          'created_at'=> date('Y-m-d h:i:s'),
          'updated_at'=>date('Y-m-d h:i:s')
        );
         $success = $this->M_Auth->register($userdetails);
         echo $success;
    }

    function login(){
        $data = $this->input->post('data');

        $userdetails = array(
          'username' =>$data['login_username'],
          'password'=> md5($data['login_password'])
        );
        $success = $this->M_Auth->login($userdetails);
       
         switch($success){
           case 'No usertype':
             echo base_url('Profile/profilesetType');
           break;
           case 'Enthusiast':
            //authentication complete, send to logged homepage
             echo base_url('Profile/profileSettings');
            break;
        
           default:
               echo 'True'; 
            break;

         }

    }
    
    function logout(){
        
        $this->session->sess_destroy();
        echo base_url('Home');
    }

}
