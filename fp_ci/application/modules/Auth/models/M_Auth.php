                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              <?php

class M_Auth extends CI_Model{


      function __construct(){
        parent::__construct();
      }

      function queryUser(){
          $sql = "Select * from users where id = ? ";
          $query = $this->db->query($sql,array($this->session->userdata('id')));
          if($query->num_rows()>0){
             return $query->row();
          }

      }
      function register($data){
          $sql = "Select * from users Where username = ? OR email = ? ";
          $query = $this->db->query($sql,array($data['username'],$data['email']));

          if($query->num_rows() > 0){
            return 'User exist please create another one.';
          }
          else{
              $query1 = $this->db->insert('users',$data);
              return 'New User has been registered!';
          }
      }

      function login($data){
          $sql = "select * from users where username = ? and password = ?  OR email = ? and password = ?";
          $query = $this->db->query($sql,array($data['username'],$data['password'],$data['username'],$data['password']));

          if($query->num_rows() > 0){

            $row = $query->row();
                   if($row->user_type==null || $row->user_type==''){

                        $session = array(
                            'id' => $row->id,
                            'username'=> $row->username,
                            'email' => $row->email,
                            'password'=> $row->password,
                            'user_type'=>$row->user_type,
                            'logged_in' => 1
                        );

                        $this->session->set_userdata($session);
                        return 'No usertype';
                   }
                   else if($row->user_type == 'Pet Enthusiast'){

                         $session = array(
                             'id' => $row->id,
                             'username'=> $row->username,
                             'email' => $row->email,
                             'password'=> $row->password,
                             'user_type'=>$row->user_type,
                             'logged_in' => 1
                         );

                         $this->session->set_userdata($session);
                         return 'Enthusiast';
                   }
                   else if($row->user_type == 'Vet Clinics' || $data['user_type']== 'Pet Shops'){
                     $session = array(
                         'id' => $row->id,
                         'username'=> $row->username,
                         'email' => $row->email,
                         'password'=> $row->password,
                         'user_type'=>$row->user_type,
                         'logged_in' => 1
                     );
                        $this->session->set_userdata($session);
                        return 'Clinics';
                   }
          }
          else{
              return 'No users found';
          }
      }
}
