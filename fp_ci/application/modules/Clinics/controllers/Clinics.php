<?php

class Clinics extends MY_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->model('Profile/M_Profile');
        $this->load->model('Auth/M_Auth');
    }
    
    function index(){
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $queryuserdata = $this->M_Auth->queryUser();
      $data = array(
          'title'=>'Take a tour for your pets',
          'content_view'=>'Home/home_v',
          'id'=> $this->session->userdata('id'),
//          'username'=> $queryuserdata->username,
//          'email'=> $queryuserdata->email,
//          'profilepicture'=>$queryprofilephoto 
      );
      $this->template->get_viewclinics($data);
    }
    
    function item(){
         $queryuserdata = $this->M_Auth->queryUser();
        $queryprofilephoto =$this->M_Profile->getprofilePhoto();
        $data = array(
          'title'=>'Profile Items',
          'content_view'=>'Home/item',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'password'=> $queryuserdata->password,
          'profilepicture'=>$queryprofilephoto
            
      );
      
      $this->template->get_viewclinicstars($data);
    }
    
    function adoption(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $data = array(
          'title'=>'Adoptions',
          'content_view'=>'Home/adoption',
          'id'=> $this->session->userdata('id'),
//          'username'=> $queryuserdata->username,
//          'email'=> $queryuserdata->email,
//          'profilepicture'=>$queryprofilephoto
        
      );
     
      $this->template->get_viewclinics($data);
    }

    function breeder(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $data = array(
          'title'=>'Breeder',
          'content_view'=>'Home/breeder',
          'id'=> $this->session->userdata('id'),
//          'username'=> $queryuserdata->username,
//          'email'=> $queryuserdata->email,
//          'profilepicture'=>$queryprofilephoto
        
      );

      $this->template->get_viewclinics($data);
    }

    function petstore(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $data = array(
          'title'=>'Pet Store',
          'content_view'=>'Home/petstore',
          'id'=> $this->session->userdata('id'),
//          'username'=> $queryuserdata->username,
//          'email'=> $queryuserdata->email,
//          'profilepicture'=>$queryprofilephoto
        
      );

      $this->template->get_viewclinics($data);
    }
    function clinics_v(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $data = array(
          'title'=>'Clinics and Shops',
          'content_view'=>'Home/clinics',
          'id'=> $this->session->userdata('id'),
//          'username'=> $queryuserdata->username,
//          'email'=> $queryuserdata->email,
//          'profilepicture'=>$queryprofilephoto
        
      );
      
      $this->template->get_viewclinics($data);
    }

    function clinicsprofile(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $data = array(
          'title'=>'Clinics Profile',
          'content_view'=>'Home/clinicsprofile',
          'css'=>'fontawesome-stars.css',
          'id'=> $this->session->userdata('id'),
//          'username'=> $queryuserdata->username,
//          'email'=> $queryuserdata->email,
//          'profilepicture'=>$queryprofilephoto
        
      );
     
      $this->template->get_viewclinicstars($data);
    }

    function membersprofile(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $data = array(
          'title'=>'Members Profile',
          'content_view'=>'Home/members-profile',
          'id'=> $this->session->userdata('id'),
//          'username'=> $queryuserdata->username,
//          'email'=> $queryuserdata->email,
//          'profilepicture'=>$queryprofilephoto
        
      );
     
      $this->template->get_viewclinics($data);
    }

    function petprofile(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $data = array(
          'title'=>'Pet Profile',
          'content_view'=>'Home/petprofile',
          'id'=> $this->session->userdata('id'),
//          'username'=> $queryuserdata->username,
//          'email'=> $queryuserdata->email,
//          'profilepicture'=>$queryprofilephoto
        
      );
     
      $this->template->get_viewclinics($data);
    }

  

    function postsale(){
        $queryuserdata = $this->M_Auth->queryUser();
        $queryprofilephoto =$this->M_Profile->getprofilePhoto();
        $data = array(
          'title'=>'Profile Post Sale',
          'content_view'=>'Home/post-sale',
          'id'=> $this->session->userdata('id'),
//          'username'=> $queryuserdata->username,
//          'email'=> $queryuserdata->email,
//          'password'=> $queryuserdata->password,
//          'profilepicture'=>$queryprofilephoto
            
      );
      
      $this->template->get_viewclinics($data);
    }
    
}