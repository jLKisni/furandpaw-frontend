<?php

class Home extends MY_Controller{

    function __construct(){
      parent::__construct();
    }

    function index(){

      $data['title'] = 'Take a tour for your pets.';
      $data['content_view'] = 'Home/home_v';
      $this->template->get_viewhome($data);
    }

    function adoption(){
      $data['title'] = 'Adoptions';
      $data['content_view'] = 'Home/adoption';
      $this->template->get_viewhome($data);
    }

    function breeder(){

      $data = array('title'=>'Breeder','content_view'=>'Home/breeder');
      $this->template->get_viewhome($data);
    }

    function petstore(){

      $data = array('title'=>'Pet Store','content_view'=>'Home/petstore');
      $this->template->get_viewhome($data);
    }
    function clinics(){

      $data = array('title'=>'Clinics and Shops','content_view'=>'Home/clinics');
      $this->template->get_viewhome($data);
    }

    function clinicsprofile(){
      $data = array('title'=>'Clinics Profile','content_view'=>'Home/clinicsprofile','css'=>'fontawesome-stars.css');
      $this->template->get_viewstars($data);
    }

    function membersprofile(){
      $data = array('title'=>'Members Profile','content_view'=>'Home/members-profile');
      $this->template->get_viewhome($data);
    }

    function petprofile(){
      $data = array('title'=>'Pet Profile','content_view'=>'Home/petprofile');
      $this->template->get_viewhome($data);
    }

    function item(){
      $data = array('title'=>'Item','content_view'=>'Home/post-sale','css'=>'fontawesome-stars.css');
      $this->template->get_viewstars($data);
    }

    function postsale(){
      $data = array('title'=>'Sale','content_view'=>'Home/post-sale');
      $this->template->get_viewhome($data);
    }
}
