<div class="header-container">

		<div class="header-content">

			<div class="header-left">
				<h1>Vet Clinics</h1>
			</div>

			<div class="header-right">
				<ol class="breadcrumb">
				  <li><a href="<?php echo base_url();?>Home">Home</a></li>
				  <li class="active">Vet Clinics</li>
				</ol>
			</div>

		</div>

	</div><!-- end of header container -->


  <div class="content-wrap clearfix">

    <div class="section-content clearfix">

        <div class="content-left">

            <div class="items-thumbnail-border">
                <div class="items-thumbnail">

                    <div class="carousel">

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="<?php echo base_url();?>assets/img/VetClinics/CVD.jpg" alt="...">
                            </div>
                            <div class="item">
                                <img src="<?php echo base_url();?>assets/img/VetClinics/CVD.jpg" alt="...">
                            </div>

                            <div class="item">
                                <img src="<?php echo base_url();?>assets/img/VetClinics/CVD.jpg" alt="...">
                            </div>

                            <div class="item">
                                <img src="<?php echo base_url();?>assets/img/VetClinics/CVD.jpg" alt="...">
                            </div>
                        </div>
                        <a class="left carousel-control" href=".carousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href=".carousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>


                </div><!--end of thumbnail-->
            </div><!--end border thumbnail-->

            <div class="bs-example bs-example-tabs" data-example-id=togglable-tabs>
                <ul id=myTabs class="nav nav-tabs" role=tablist>
                    <li role=presentation class=active> <a href=#desc id=desc-tab role=tab data-toggle=tab aria-controls=home aria-expanded=true>Description</a> </li>
                    <li role=presentation> <a href=#reviews role=tab id=reviews-tab data-toggle=tab aria-controls=profile>Reviews (2)</a> </li>
                </ul>
                <div id=myTabContent class=tab-content style="padding:30px 20px; border: 1px solid #e7e7e7">
                    <div role=tabpanel class="tab-pane fade in active" id=desc aria-labelledby=desc-tab>
                        <h3 style="font-weight:700">Clinic Schedule</h3>
                        <br>
                        <p> The clinic offers low-cost services and is open to the public and indigent pet owners on Mondays, Tuesdays, Thursdays, Fridays and Saturdays at 1:30 - 3:00 PM.</p>
                        <h3 style="font-weight:700">Clinic Rates</h3>
                        <br>
                        <p>Spay & Neuter</p>
                        <p><span class="fa fa-check-circle-o blue"></span>  Female cat : 1,000.00</p>
                        <p><span class="fa fa-check-circle-o blue"></span> Female dog : 1,500.00 (additional P500 for every 10 kg in excess of 15 kg bodyweight)</p>
                        <p><span class="fa fa-check-circle-o blue"></span>  Male cat: 700.00</p>
                        <p><span class="fa fa-check-circle-o blue"></span> Male dog : 1,000.00 (additional P500 for every 10 kg in excess of 15 kg body weight)</p>
                        <br>
                        <h4>Services Offered</h4>
                        +24 hours on emergency cases,
                        Shuttle & Pet Ambulance,
                        Grooming,
                        Consultation,
                        laboratory,
                        x-ray,
                        ultrasound,
                        ECG,
                        Surgery,
                        dental care,
                        boarding,
                        confinement,
                        pet pharmacy,
                        pet accessories,
                        grooming,
                        animal health products,
                        Cat Clinic.
                        <hr>
                        <h4>Document photos</h4>
                        <br>
                        <div class="thumbnails">
                            <img src="<?php echo base_url();?>assets/img/thumbnail/karen.jpg" />
                            <img src="<?php echo base_url();?>assets/img/thumbnail/zetta.jpg" />
                            <img src="<?php echo base_url();?>assets/img/thumbnail/lyndon.jpg" />
                            <img src="<?php echo base_url();?>assets/img/thumbnail/default-image.png"/>

                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <h4>Location</h4>
                        <div style="text-decoration:none; overflow:hidden; height:300px; max-width:100%;">
                            <div id="embed-map-canvas" style="height:100%; width:100%;max-width:100%;">
                                <iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Cebu+Veterinary+Doctor+Ramos+Cebu+City&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <!--for description-->
                    <div role=tabpanel class="tab-pane fade" id=reviews aria-labelledby=reviews-tab>
                        <div class="reviews-content">
                            <h3 style="font-weight:300">(2) Review for this item</h3>
                            <br>
                            <ol class="commentlist">
                                <li>
                                    <div class="comment-16">
                                        <img src="<?php echo base_url();?>assets/img/thumbnail/default-image.png" class="avatar">

                                        <div class="comment-text">
                                            <div class="star-rating">
                                                <select class="star" id="star1">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>

                                            <p class="meta">
                                                <strong>John Louise Berdida</strong>
                                                -
                                                <time>Posted: 1 Hour ago:</time>
                                            </p>
                                            <div class="description">
                                                Batiag nawng ana imong pets oie . .Bdw interesting ko nga mopalit ana niya kay ipakaon nako sa akong binuhi nga dragon
                                                mo ask lang ta ko kung dili bana matakdan sa bun.i akong dragon nig kaon ana niya kay gi bun.i raba na imong pets or
                                                tambalan lang sa nako na ang lubot ana imong pets usa akong ipakaon ?
                                            </div>
                                        </div>
                                    </div>

                                </li> <!--end of first comment-->

                                <li>
                                    <div class="comment-16">
                                        <img src="<?php echo base_url();?>assets/img/thumbnail/zetta.jpg" class="avatar">

                                        <div class="comment-text">
                                            <div class="star-rating">
                                                <select class="star">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>

                                            <p class="meta">
                                                <strong>Pet ni John</strong>
                                                -
                                                <time> Posted 32 minutes ago</time>
                                            </p>
                                            <div class="description">
                                                Ka hinawayon ba nimong animala ka matay paka mura kag gwapo da . . tseee ayaw nalang mi palita pak u nimo ./.
                                            </div>
                                        </div>
                                    </div>

                                </li> <!--end of first comment-->
                            </ol>
                            <div class="clearfix"></div>
                        </div>

                        <div>
                            <h4>Add Review</h4>
                            <br>
                            <p>Your email address will not be published. Required fields are marked <span style="color:red">*</span></p>
                            <p class="comment-form-rating">
                                <label for="">Your Rating</label>
                            <div class="comment-star-rating">
                                <select class="star">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                            <textarea name="" cols="10" rows="6" class="form-control" placeholder="Send your message here....."></textarea>
                            <br>
                            <div class="review-input">

                                <div class="review-label">
                                    <label>Name: <span style="color:red">*</span></label>
                                </div>

                                <div class="review-inputs">
                                    <input type="text" placeholder="Your name here..."/>
                                </div>

                                <div class="review-label">
                                    <label>Email: <span style="color:red">*</span></label>
                                </div>

                                <div class="review-inputs">
                                    <input type="text" placeholder="John Doe@yahoo.com"/>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                            <br>
                            <center><button class="button-search hvr-pulse">Submit Message</button></center>
                            <div class="clearfix"></div>
                            </p>
                        </div>


                    </div>
                    <!--for reviews-->
                </div>
            </div>

        </div> <!-- dir-form -->




        <div class="widget-right-details">

            <div class="widget-navbar">
               Cebu Veterinary Doctor
            </div>

            <div class="widget-content">
                <div class="widget-price"><span class="fa fa-map-marker fa-lg" style="padding-right:3px;"></span>  Location: Fuente Osmena Cebu City</div>
                <hr>
                <div class="widget-number"><span class="fa fa-mobile-phone fa-2x" style="padding-right:8px;"> </span> Contact no: 09123456789</div>
                <hr>
                <div class="widget-number"><span class="fa fa-calendar fa-lg" style="padding-right:8px;"> </span> Member Since: Feb. 07,2016</div>
                <hr>
                <textarea name="" id="" cols="2" rows="3" class="form-control" placeholder="Send your message here....."></textarea>
                <br>
                <center><button class="button-submit hvr-pulse-shrink" onclick="bookmarks()">Send Message</button></center>
            </div>

        </div>

        <div class="widget-right-details" style="margin-top:20px;">

            <div class="widget-navbar">Member Details</div>

            <div class="widget-content">
                <div class="item-avatar">
                    <a href="#">
                        <img src="<?php echo base_url(); ?>assets/img/VetClinics/CVD.jpg" alt="...">
                    </a>
                </div>

                <div class="custom-item">
                    <div class="item-title">
                        <a href="#">Cebu Veterinary Doctors</a>
                    </div>

                    <div class="item-meta">
                        <span class="fa fa-map-marker" style="padding:0 2px"> </span> Cebu City
                    </div>

                </div>

            </div>

        </div>

    </div>
</div>
