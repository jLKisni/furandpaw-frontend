<?php

class IProfile extends MY_Controller{
    function __construct(){
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->model('Profile/M_Profile');
    }
   
    
     function addPets(){
        $filename = 'getphotos';
        $path = "./assets/img/uploaded_pets_image";

        chmod($path, 777);
        $config["upload_path"] = $path;
        $config["allowed_types"] = "jpg|png|jpeg";
        $config["max_size"] = "6000";
        $config["max_height"] = "7680";
        $config["max_width"] = "10240";

        $this->load->library("upload",$config);
        $this->upload->do_upload($filename);
        $petphoto = $this->upload->data();

        $petname = $this->input->post('petname');
        $breed = $this->input->post('breed');
        $age = $this->input->post('age');
        $gender = $this->input->post('gender');
        $description = $this->input->post('description');
        $color =$this->input->post('color');


        $petdetails = array(
            'petname'=>$petname,
            'breed'=>$breed,
            'age'=>$age,
            'gender'=>$gender,
            'color' =>$color,
            'description'=>$description,
            'image'=>$petphoto['file_name'],
            'image_name'=>$petphoto['raw_name'],
            'imageable_type'=>'Pet_photo',
             'created_at'=>date('Y-m-d h:i:s'),
             'updated_at'=>date('Y-m-d h:i:s')
            );

          $this->M_Profile->addPets($petdetails);

          redirect('Profile/profileviewPets');


    }
    
    function uploadPhotoImage(){
        $filename = 'getphotos';
        
        $path = "./assets/img/uploaded_image/";
        chmod($path, 777);
        $config["upload_path"] = $path;
        $config["allowed_types"] = "jpg|png|jpeg";
        $config["max_size"] = "6000";
        $config["max_height"] = "7680";
        $config["max_width"] = "10240";

        $this->load->library("upload", $config);
        //$this->upload->do_upload("getphotos");

        if( ! $this->upload->do_upload($filename)){
            //$error = array("error" => $this->upload->display_errors());

            //$this->load->view();

            echo "failed";
        }else{
            
            $data = $this->upload->data();
            $imagedetails = array(
             'image' =>$data['file_name'],
             'image_name'=>$data['raw_name'],
             'imageable_id'=>$this->session->userdata('id'),
             'imageable_type'=>'Profile_photo',
             'created_at'=>date('Y-m-d h:i:s'),
             'updated_at'=>date('Y-m-d h:i:s')
               
           );
            $this->M_Profile->uploadPhotoImage($imagedetails);
            redirect('Profile/profilechangePhoto');
  
        }

    
    }
    
    function uploadPhotoCover(){
        $filename = 'getcover';
        
        $path = "./assets/img/uploaded_image/";
        chmod($path, 777);
        $config["upload_path"] = $path;
        $config["allowed_types"] = "jpg|png|jpeg";
        $config["max_size"] = "6000";
        $config["max_height"] = "7680";
        $config["max_width"] = "10240";
        $this->load->library("upload", $config);
        if(!$this->upload->do_upload($filename)){
            echo "failed";
        }else{
            
            $data = $this->upload->data();
            $imagedetails = array(
             'image' =>$data['file_name'],
             'image_name'=>$data['raw_name'],
             'imageable_id'=>$this->session->userdata('id'),
             'imageable_type'=>'Profile_cover',
             'created_at'=>date('Y-m-d h:i:s'),
             'updated_at'=>date('Y-m-d h:i:s')
               
           );
            $this->M_Profile->uploadPhotoImage($imagedetails);
            redirect('Profile/profilechangeCover');
  
        }

    }
    
    function deleteprofilePhoto(){
         $data = $this->input->post('items');
         $userdetails = array(
             'id'=>$data,
             'type'=>'Profile_photo'
         );
         $this->M_Profile->deletephoto($userdetails);
        echo base_url('Profile/profilechangePhoto');
    }
    
    function deleteprofileCover(){
         $data = $this->input->post('items');
         $userdetails = array(
             'id'=>$data,
             'type'=>'Profile_cover'
         );
         $this->M_Profile->deletephoto($userdetails);
        echo base_url('Profile/profilechangeCover');
    }
}
