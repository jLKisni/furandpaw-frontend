<?php

class Profile extends MY_Controller{

    function __construct(){
      parent::__construct();
      $this->load->model('Profile/M_Profile');
      $this->load->model('Auth/M_Auth');
      $this->load->model('Profile/Pets_pagination');

       if(!$this->session->userdata('id')){
            redirect('Home');
        }
     
    }

    function index(){
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $queryuserdata = $this->M_Auth->queryUser();
      $data = array(
          'title'=>'Take a tour for your pets',
          'content_view'=>'Home/home_v',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'profilepicture'=>$queryprofilephoto 
      );
      $this->template->get_viewprofile($data);
    }

    function adoption(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $data = array(
          'title'=>'Adoptions',
          'content_view'=>'Home/adoption',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'profilepicture'=>$queryprofilephoto
        
      );
     
      $this->template->get_viewprofile($data);
    }

    function breeder(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $data = array(
          'title'=>'Breeder',
          'content_view'=>'Home/breeder',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'profilepicture'=>$queryprofilephoto
        
      );

      $this->template->get_viewprofile($data);
    }

    function petstore(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $data = array(
          'title'=>'Pet Store',
          'content_view'=>'Home/petstore',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'profilepicture'=>$queryprofilephoto
        
      );

      $this->template->get_viewprofile($data);
    }
    function clinics(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $data = array(
          'title'=>'Clinics and Shops',
          'content_view'=>'Home/clinics',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'profilepicture'=>$queryprofilephoto
        
      );
      
      $this->template->get_viewprofile($data);
    }

    function clinicsprofile(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $data = array(
          'title'=>'Clinics Profile',
          'content_view'=>'Home/clinicsprofile',
          'css'=>'fontawesome-stars.css',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'profilepicture'=>$queryprofilephoto
        
      );
     
      $this->template->get_viewprofilestars($data);
    }

    function membersprofile(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $data = array(
          'title'=>'Members Profile',
          'content_view'=>'Home/members-profile',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'profilepicture'=>$queryprofilephoto
        
      );
     
      $this->template->get_viewprofile($data);
    }

    function petprofile(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $data = array(
          'title'=>'Pet Profile',
          'content_view'=>'Home/petprofile',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'profilepicture'=>$queryprofilephoto
        
      );
     
      $this->template->get_viewprofile($data);
    }

    function item(){
         $queryuserdata = $this->M_Auth->queryUser();
        $queryprofilephoto =$this->M_Profile->getprofilePhoto();
        $data = array(
          'title'=>'Profile Items',
          'content_view'=>'Home/item',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'password'=> $queryuserdata->password,
          'profilepicture'=>$queryprofilephoto
            
      );
      
      $this->template->get_viewprofilestars($data);
    }

    function postsale(){
        $queryuserdata = $this->M_Auth->queryUser();
        $queryprofilephoto =$this->M_Profile->getprofilePhoto();
        $data = array(
          'title'=>'Profile Post Sale',
          'content_view'=>'Home/post-sale',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'password'=> $queryuserdata->password,
          'profilepicture'=>$queryprofilephoto
            
      );
      
      $this->template->get_viewprofile($data);
    }
    
   function profileEdit(){
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $queryprofilecover =$this->M_Profile->getprofileCover();
       $queryenthusiastData = $this->M_Profile->getenthusiastData();
      if($queryenthusiastData == false){
  
          $data = array(
          'title'=>'Profile Edit',
          'content_view'=>'Profile/profile-edit',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'password'=> $queryuserdata->password,
          'profilepicture'=>$queryprofilephoto,
          'profilecover'=>$queryprofilecover,
          'name'=> '',
          'gender' => '',
          'address'=>'',
          'contactnumber'=>''
      );

       $this->template->get_viewprofile($data);
      }
     else{
          
        $data = array(
          'title'=>'Profile Edit',
          'content_view'=>'Profile/profile-edit',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'password'=> $queryuserdata->password,
          'profilepicture'=>$queryprofilephoto,
          'profilecover'=>$queryprofilecover,
          'name'=> $queryenthusiastData->name,
          'gender' => $queryenthusiastData->gender,
          'address'=>$queryenthusiastData->address,
          'contactnumber'=>$queryenthusiastData->contact_number
      );

       $this->template->get_viewprofile($data);
    }
     
    }
    function profilesetType(){
     
      $queryuserdata = $this->M_Auth->queryUser();
      $queryprofilephoto =$this->M_Profile->getprofilePhoto();
      $queryprofilecover =$this->M_Profile->getprofileCover();
        $data = array(
          'title'=>'Profile Type',
          'content_view'=>'Profile/profile-setType',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'password'=> $queryuserdata->password,
          'profilepicture'=>$queryprofilephoto,
          'profilecover'=>$queryprofilecover
      );
      $this->template->get_viewprofile($data);
    }
    
    function profileSettings(){
      
        $queryuserdata = $this->M_Auth->queryUser();
        $queryprofilephoto =$this->M_Profile->getprofilePhoto();
        $queryprofilecover =$this->M_Profile->getprofileCover();
        $data = array(
          'title'=>'Profile Settings',
          'content_view'=>'Profile/profile-settings',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'password'=> $queryuserdata->password,
          'profilepicture'=>$queryprofilephoto,
          'profilecover'=>$queryprofilecover
      );
        $this->template->get_viewprofile($data);
    }
    
    function profileDelete(){
        $queryuserdata = $this->M_Auth->queryUser();
        $queryprofilephoto =$this->M_Profile->getprofilePhoto();
        $queryprofilecover =$this->M_Profile->getprofileCover();
        $data = array(
          'title'=>'Profile Delete',
          'content_view'=>'Profile/profile-delete',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'password'=> $queryuserdata->password,
          'profilepicture'=>$queryprofilephoto,
          'profilecover'=>$queryprofilecover
            
      );
       
        $this->template->get_viewprofile($data);
    }
    
    function profileActivity(){
        $queryuserdata = $this->M_Auth->queryUser();
        $queryprofilephoto =$this->M_Profile->getprofilePhoto();
        $queryprofilecover =$this->M_Profile->getprofileCover();
        $data = array(
          'title'=>'Profile Activity',
          'content_view'=>'Profile/profile-activity',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'password'=> $queryuserdata->password,
          'profilepicture'=>$queryprofilephoto,
          'profilecover'=>$queryprofilecover
      );
       
        $this->template->get_viewprofile($data);
    }
    
    function profileAddpets(){
        $queryuserdata = $this->M_Auth->queryUser();

        $data = array(
          'title'=>'Profile Add Pets',
          'content_view'=>'Profile/profile-addpets',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'password'=> $queryuserdata->password,
         
      );
       
        $this->template->get_viewprofile($data);
    }
    
    function profilechangePhoto(){
        $queryuserdata = $this->M_Auth->queryUser();
        $queryprofilephoto =$this->M_Profile->getprofilePhoto();
        $queryprofilecover =$this->M_Profile->getprofileCover();
        $data = array(
          'title'=>'Profile Change Photos',
          'content_view'=>'Profile/profile-changephoto',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'password'=> $queryuserdata->password,
          'profilepicture'=>$queryprofilephoto,
          'profilecover'=>$queryprofilecover
      );
   
        $this->template->get_viewprofile($data);
    }
    
    function profilechangeCover(){
        $queryuserdata = $this->M_Auth->queryUser();
        $queryprofilephoto =$this->M_Profile->getprofilePhoto();
        $queryprofilecover =$this->M_Profile->getprofileCover();
        $data = array(
          'title'=>'Profile Change Cover Photos',
          'content_view'=>'Profile/profile-changecover',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'password'=> $queryuserdata->password,
          'profilepicture'=>$queryprofilephoto,
          'profilecover'=>$queryprofilecover
          
      );
    
        $this->template->get_viewprofile($data);
    }
    
    function profilepetProfile(){
        $queryuserdata = $this->M_Auth->queryUser();
        
        $data = array(
          'title'=>'Profile Profile',
          'content_view'=>'Profile/profile-petprofile',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'password'=> $queryuserdata->password,
      );
        
        $this->template->get_viewprofile($data);
    }
    
    function profileviewPets(){
        $this->load->library('pagination');
        $config =array();
        $config['base_url']=  base_url().'Profile/profileviewPets';
        $config['total_rows'] = $this->Pets_pagination->count_pets();
        $config['per_page'] = 2;
        $config['num_links'] = 3;
        $config['display_page'] = FALSE;
        
        $config['num_tag_open']=' <a class="page-numbers">';
        $config['num_tag_close']='</a>';
        $config['cur_tag_open'] ='<a class="page-numbers" style="margin-left:5px;">';
        $config['cur_tag_close']='</a>';
        $config['next_link']='<a class="page-numbers"><span class="fa fa-long-arrow-right"></span></a>';
        $config['prev_link']='<a class="page-numbers"><span class="fa fa-long-arrow-left"></span></a>';
        
        $this->pagination->initialize($config);   
        $petsdata =$this->Pets_pagination->fetch_pets($config['per_page'],$this->uri->segment(3));  
        //echo print_r($petsdata);
        $queryuserdata = $this->M_Auth->queryUser();
        $queryprofilephoto =$this->M_Profile->getprofilePhoto();
        $queryprofilecover =$this->M_Profile->getprofileCover();
        $data = array(
          'title'=>'Profile View Pets',
          'content_view'=>'Profile/profile-viewpets',
          'id'=> $this->session->userdata('id'),
          'username'=> $queryuserdata->username,
          'email'=> $queryuserdata->email,
          'password'=> $queryuserdata->password,
          'profilepicture'=>$queryprofilephoto,
          'profilecover'=>$queryprofilecover,
          'pets' => $petsdata,
          'links'=> $this->pagination->create_links(),
          'count_pets'=>$this->Pets_pagination->count_pets(),
       
       
      );
      
        $this->template->get_viewprofile($data);
        }
    
    
    
}
