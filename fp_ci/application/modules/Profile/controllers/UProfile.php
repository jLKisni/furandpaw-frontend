<?php

class UProfile extends MY_Controller{
    
   function __construct() {
       parent::__construct();
       
       $this->load->model('Profile/M_Profile');
   }
   
    function setuserType(){
        $usertype = $this->input->post('usertype');
        $data = array(
            'id'=>$this->session->userdata('id'),
            'usertype' =>$usertype,
            'updated_at'=>date('Y-m-d h:i:s')
            );
        $this->M_Profile->setType($data);
         redirect('Profile/profileSettings');
        
    }
    
    function updateSettings(){
        $data = $this->input->post('data');
        $userdetails = array(
            'id'=>$this->session->userdata('id'),
            'email'=>$data['email'],
            'password'=>$data['newpass'],
            'updated_at'=>date('Y-m-d h:i:s')
            );
            $success= $this->M_Profile->updateSettings($userdetails);
          echo $success;
    }
    
    function updateSettingsNp(){
        $data2 = $this->input->post('items');
        $userdetails = array(
            'id'=>$this->session->userdata('id'),
            'email'=>$data2['email'],
            'updated_at'=>date('Y-m-d h:i:s')
        );
        $success = $this->M_Profile->updateSettingsNp($userdetails);
        echo $success;
    }
    
     function updateprofileEdit(){
        $name = $this->input->post('name');
        $gender = $this->input->post('gender');
        $address = $this->input->post('address');
        $contactnumber = $this->input->post('contactnumber');

        $userdetails = array(
            'name' => $name,
            'gender' => $gender,
            'address'=> $address,
            'contactnumber'=>$contactnumber,
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
            );

        // print_r($userdetails);
        $success = $this->M_Profile->updateprofileEdit($userdetails);
        redirect('Profile/profileEdit');

    }
}
