<?php

class M_Profile extends CI_Model{


      function __construct(){
        parent::__construct();

      }
      
     function getenthusiastData(){
       $sql = "Select * from enthusiast where user_id = ?";  
       $query = $this->db->query($sql,array($this->session->userdata('id')));
        if($query->num_rows()>0){
            $row = $query->row();
            return $row;
        }
        else{
            return false;
        }
     } 
      
     function setType($data){
          
          $sql = "update users SET user_type = ?,updated_at = ? WHERE id = ?";
          $query = $this->db->query($sql,array($data['usertype'],$data['updated_at'],$data['id']));
     }
     
     function updateSettings($data){
          $sql = "update users SET email = ?, password = ? , updated_at = ? WHERE id = ?";
          $query = $this->db->query($sql,array($data['email'],md5($data['password']),$data['updated_at'],$data['id']));
          return 'Your Password And Email Address Successfully Updated';
      
     }
     
     function updateSettingsNp($data){
         $sql = "update users SET email = ? , updated_at = ? WHERE id = ?";
         $query = $this->db->query($sql,array($data['email'],$data['updated_at'],$data['id']));
         return 'Your Email Address Successfully Updated';
     }
     
   
     
     function uploadPhotoImage($data){
         $this->db->where('imageable_id',$data['imageable_id']);
         $this->db->where('imageable_type',$data['imageable_type']);
         $query = $this->db->get('images');
         
         if($query->num_rows()>0){
              $setdata = array(
                'img_name' => $data['image_name'],
                'image' => $data['image'],
                'updated_at' => $data['updated_at']
                );
          
                $this->db->where('imageable_id',$data['imageable_id']);
                $this->db->where('imageable_type',$data['imageable_type']);
                $this->db->update('images', $setdata);
         }
         else{
              $sql = "insert into images (img_name,image,created_at,updated_at,imageable_type,imageable_id)values(?,?,?,?,?,?)";
              $query1 = $this->db->query($sql,array($data['image_name'],$data['image'],$data['created_at'],$data['updated_at'],$data['imageable_type'],$data['imageable_id']));
         }
     }
     
   
     function getprofilePhoto(){ 
        $sql = "Select * from images where imageable_id = ? And imageable_type = 'Profile_photo'";
          $query = $this->db->query($sql,array($this->session->userdata('id')));
          if($query->num_rows()>0){
              $row = $query->row();
             return $row->image;
          }
          else{
             return 'default-image.png';
          }
     }
     
     function getprofileCover(){ 
          $sql = "Select * from images where imageable_id = ? And imageable_type = 'Profile_cover'";
          $query = $this->db->query($sql,array($this->session->userdata('id')));
          if($query->num_rows()>0){
              $row = $query->row();
             return $row->image;
          }
          else{
             return 'cover-default.png';
          }
     }
     
     function deletephoto($data){
         $this->db->where('imageable_id',$data['id']);
         $this->db->where('imageable_type',$data['type']);
         $this->db->delete('images');
     
     }
     
     
      function updateprofileEdit($userdetails){
        $setdata = array(
            'name' => $userdetails['name'],
            'gender' => $userdetails['gender'],
            'address'=> $userdetails['address'],
            'contact_number'=>$userdetails['contactnumber'],
            'updated_at' => $userdetails['updated_at'],
            'user_id' => $this->session->userdata('id')
          );

         $this->db->where('user_id',$setdata['user_id']);
         $query = $this->db->get('enthusiast');

         if($query->num_rows()>0){

          $newdata = array(
            'name' => $userdetails['name'],
            'gender' => $userdetails['gender'],
            'address'=> $userdetails['address'],
            'contact_number'=>$userdetails['contactnumber'],
            'updated_at' => $userdetails['updated_at']     
          );

          $this->db->where('user_id',$setdata['user_id']);
          $this->db->update('enthusiast', $newdata);

         }
         else{

          $this->db->insert('enthusiast',$setdata);

         }
        
     }
     
     function addPets($petdetails){
        $petData = array(
          'name'=>$petdetails['petname'],
            'breed'=>$petdetails['breed'],
            'age'=>$petdetails['age'],
            'gender'=>$petdetails['gender'],
            'color' =>$petdetails['color'],
            'description'=>$petdetails['description'],
            'created_at'=>$petdetails['created_at'],
             'updated_at'=>$petdetails['updated_at'],
             'ownable_id'=>$this->session->userdata('id')
          );
          $this->db->insert('pets',$petData);//insert pets
          $petid = $this->db->insert_id();
                $petphoto = array(
                   'img_name'=>$petdetails['image_name'],
                   'image'=>$petdetails['image'],
                   'created_at'=>$petdetails['created_at'],
                   'updated_at'=>$petdetails['updated_at'],
                   'imageable_type'=>$petdetails['imageable_type'],
                   'imageable_id'=>$petid //pet id
                );
                $this->db->insert('images',$petphoto);       
       
     }

}
