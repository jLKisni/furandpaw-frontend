<?php

class Pets_pagination extends CI_Model{
    
        function __construct(){
            parent::__construct();
        }
        
       public function count_pets(){
            return $this->db->count_all('pets');
        }
        
       public function fetch_pets($limit,$offset){
           $this->db->limit($limit,$offset);
           $this->db->select('*');
           $this->db->from('pets');
           $this->db->join('images','images.imageable_id = pets.id');
           $query = $this->db->get();
           if($query->num_rows>0){
               return $query->result();
           }
           else{
               return $query->result();
           }
       }
       
       function fetch_petsimage($id){
           $sql = "select * from images where imageable_id = ?";
           $query = $this->db->query($sql,array($id));
           $row = $query->row();
            return $row->image;
       }
}

