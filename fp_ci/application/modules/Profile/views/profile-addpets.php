<div class="header-container">

		<div class="header-content">

			<div class="header-left">
				<h1>Pet Adding</h1>
			</div>

			<div class="header-right">
				<ol class="breadcrumb">
				  <li><a href="<?php echo base_url();?>Profile">Home</a></li>
				  <li class="active">Profile</li>
				</ol>
			</div>

		</div>

</div><!-- end of header container -->


<div class="content-wrap">

    <div class="section-content" style="width:75%; margin:0 auto;padding:0 40px">
            <h3>Add Details for your lovely pet </h3>
            <hr>
    <form action="<?php echo base_url('Profile/IProfile/addPets');?>" method="post" enctype="multipart/form-data">
        <div class="review-input" >

            <div class="review-label">
                <label>Pet Name: <span style="color:red">*</span></label>
            </div>

            <div class="review-inputs">
                <input type="text" name="petname" placeholder="Name of your pet...">
            </div>

            <div class="review-label">
                <label>Breed: <span style="color:red">*</span></label>
            </div>

            <div class="review-inputs">
                <input type="text" name="breed" placeholder="Breed..">
            </div>

            <div class="review-label">
                <label>Age: <span style="color:red">*</span></label>
            </div>

            <div class="review-inputs">
                <input type="text" name="age" placeholder="Age of your pet..">
            </div>

            <div class="review-label">
                <label>Pet Gender: <span style="color:red">*</span></label>
            </div>

            <div class="review-inputs">
                <select style="width:15%" name="gender">
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
            </div>


            <div class="review-label">
                <label>Pet Color: <span style="color:red">*</span></label>
            </div>

            <div class="review-inputs">
                <input type="text" name="color" placeholder="Pet Color....">
            </div>


            <div class="review-label">
                <label>Upload photos: <span style="color:red">*</span></label>
            </div>

            <div class="review-inputs">
                <input type="file" name="getphotos">
            </div>

            <div class="clearfix"></div>

            <h4>Add some description</h4>
            <textarea class="form-control" rows="7" cols="10" name="description" placeholder="Add some description...."></textarea>

            <br>
            <button class="btn-comment pull-left" type="submit">Save Changes</button>


        </div>
        </form>
        <div class="clearfix"></div>
    </div>

</div>