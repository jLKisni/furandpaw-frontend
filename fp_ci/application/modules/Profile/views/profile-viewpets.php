<div class="header-container">

		<div class="header-content">

			<div class="header-left">
                            <h1><?php echo ucfirst($username);?></h1>
			</div>

			<div class="header-right">
				<ol class="breadcrumb">
				  <li><a href="<?php echo base_url();?>Profile">Home</a></li>
				  <li class="active">Profile</li>
				</ol>
			</div>

		</div>

</div><!-- end of header container -->


<div class="content-wrap clearfix">

    <div class="section-content clearfix">

        <div class="content-left">

            <article>
                <section>
                    <div id="buddypress">
                        <div id="item-header">
                            <div id="cover-image-container">
                                <a href="#" id="header-cover-image"><img src="<?php echo base_url('assets/img/uploaded_image/'.$profilecover);?>"></a>
                                <div id="item-header-cover-image">
                                    <div id="item-header-avatar">
                                        <a href="#" ><img src="<?php echo base_url('assets/img/uploaded_image/'.$profilepicture);?>" class="avatar avatar-150 photo"/></a>
                                    </div>

                                    <div id="item-header-content">
                                        <h2 class="user-nicename"><?php echo '@'.ucfirst($username);?></h2>
                                        <div id="item-buttons"></div>
                                        <span class="activity">active 1 day, 20 hours ago</span>
                                        <div id="item-meta">
                                            <div id="latest-update"></div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div> <!--item-header-->

                        <div id="item-nav">
                            <div class="item-list-tabs no-ajax" id="objectnav" role="navigation">
                                <ul>
                                    <li id="activity-personal-li" class="current">
                                        <a href="<?php echo base_url();?>Profile/profileActivity" id="user-activity">Activity</a>
                                    </li>
                                    <li id="xprofile-personal-li" class="current select">
                                        <a id="user-xprofile" href="<?php echo base_url();?>Profile/profileSettings">Profile</a>
                                    </li>
                                    <li id="friends-personal-li" class="current">
                                        <a id="user-friends" href="#">Friends <span class="count">1</span></a>
                                    </li>
                                    <li id="groups-personal-li" class="current">
                                        <a id="user-groups" href="<?php echo base_url();?>Profile/profileSettings">Settings</a>
                                    </li>
                                </ul>
                            </div>
                        </div><!--item-nav-->

                        <div id="item-body">

                            <div ><a href="<?php echo base_url();?>Profile/profileEdit">Edit</a></div>
                            <div><a href="<?php echo base_url();?>Profile/profileviewPets" class="current-selected">View Pets</a></div>
                            <div><a href="<?php echo base_url();?>Profile/profilechangePhoto">Change Profile Photo</a></div>
                            <div><a href="<?php echo base_url();?>Profile/profilechangeCover">Change Cover Image</a></div>
                            <h3>List of Pets</h3>
                            <p>Your Pets will be visible for this site. </p>

                        </div>

                        <div class="clearfix"></div>
                        <br><br>
                        <a href="<?php echo base_url('Profile/profileaddPets');?>" class="btn-comment">Add Pets</a>
                        <span style="margin-left:10px;">
                        <a href="profile-addpets.html" class="btn-comment">Sell Pets or Accessories</a>
                        </span>
                        <br>
                        <div class="clearfix"></div>
                       
                        <div class="item-list-tabs clearfix">
                            <div class="custom-pagination">
                                We found <?php echo $count_pets;?> Results
                            </div>


                           <div class="pagination-links">
                                <?php echo $links; ?>
                            </div>
                         
                        </div>

                        <?php $count = 0;?>
                      
                        
                           
                        <ul class="items-list">
                            <div class="alert alert-warning" role="alert" hidden>Sorry,no Pets were found.</div>
                            <?php foreach ($pets as $row) {?>
                            <?php echo '<li class="'.(++$count%2?"odd":"even").'">'?>
                            <div class="item-thumbnail pull-left">
                                    <a href="petprofile.html">
                                        <img src="<?php echo base_url('assets/img/uploaded_pets_image/'.$row->image);?>" alt="...">
                                    </a>
                                </div>

                                <div class="custom-item">
                                    <div class="item-title">
                                        <a href="petprofile.html"><?php echo $row->name; ?></a>
                                    </div>

                                    <div class="item-meta">
                                        <span class="activity"><?php echo $row->breed; ?></span>
                                    </div>

                                    <div class="item-meta">
                                        <span class="activity"><?php echo $row->age; ?></span>
                                    </div>


                                    <div class="item-meta">
                                        <a href="#" style="margin-right:10px">View</a><span><a href="#">Edit</a></span>
                                    </div>

                                </div>

                            </li>
                             <?php }?>


                        </ul>

                        
                        <div class="item-list-tabs clearfix">
                            <div class="custom-pagination">
                                We found <?php echo $count_pets;?> Results
                            </div>


                           <div class="pagination-links">
                                <?php echo $links; ?>
                            </div>
                         
                        </div>
                       

                    </div>
                </section>
                <hr>


            </article>



        </div>


        <div class="widget-right">
            <aside class="widget">
                <h5 class="widget_title">
                    Search Members
                </h5>

                <form class="standard-form">
                    <div>
                        <label>Name</label>
                        <input type="text"/>
                    </div>
                    <div>
                        <label>Age</label>
                        <input type="text"/>
                    </div>
                    <div>
                        <label>Pet Breed</label>
                        <select>
                            <option>--Select Breed--</option>
                            <option>German Shepherd</option>
                            <option>Askal</option>
                            <option>Pussy</option>
                            <option>lang mananap</option>
                        </select>
                    </div>
                    <div class="submit">
                        <button class="button-search hvr-pulse">SEARCH</button>
                    </div>
                </form>

            </aside>

        </div>

    </div>
</div> <!--content-wrap-->