<?php

class Template extends MY_Controller{

    function __construct(){
      parent::__construct();
    }

    function get_viewhome($data=NULL){
        $this->load->view('home_templatev',$data);
    }
    function get_viewstars($data=NULL){
        $this->load->view('home_stars',$data);
    }
    function get_viewprofile($data=NULL){
        $this->load->library('pagination');
        $this->load->view('profile_templatev',$data);
    }
    function get_viewprofilestars($data=NULL){
        $this->load->view('profile_stars',$data);
    }
    function get_viewclinics($data=NULL){
        $this->load->view('clinics_templatev',$data);
    }
    function get_viewclinicstars($data=NULL){
        $this->load->view('clinics_stars',$data);

    }
}
