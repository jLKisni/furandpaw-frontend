<!DOCTYPE html>
<html lang="en">
<head>

		  <meta charset="utf-8">
   		  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	  <meta name="viewport" content="width=device-width, initial-scale=1">
    	  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/mainstyle.css">
    	  <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/furpawlogo.png">
   	  <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
          <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/<?php echo $css; ?>">
	  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/thumbnail.css">
   	  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
   	  <link href="<?php echo base_url();?>assets/css/hover-min.css" rel="stylesheet" media="all">
   	  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    	  <title>FurAndPaws | <?php echo $title; ?></title>
	<script type="text/javascript">
	   $(function() {
		$('.star').barrating({
		theme: 'fontawesome-stars',
                });

            });
	</script>


</head>

<body>
  <!-- Modals	 -->

  <!--Login Modal -->
  <div id="login-modal">

  	<div class="login-content">
  		<div class="login-header">
  			<div id="login-left" class="login-header-left">

  				<h4><i class="fa fa-key"></i> LOG IN</h4>
  			</div>
  			<div id="login-right" class="login-header-right unselect">

  				<h4><i class="fa fa-users"></i> CREATE AN ACCOUNT</h4>
  			</div>

  			<div class="clearfix"></div>

  			<div class="login-main-content">

            <div class="content-signin">
                  <div class="form-left">
  									<form id="login-form">


  									<div class="field_username">
  										<input type="text" placeholder="Username or Email" id="login_username" name="login_username" required/>
  									</div>

  									<div class="field_password">
  										<input type="password" placeholder="Password" id="login_password" name="login_password" required/>
  									</div>


  									<input type="checkbox">
  									<span><a href="#" class="a-style">Forgot password ?</a></span>
  									<br>
  									<div class="clearfix"></div>
										</form>

  									<button type="submit" class="button_login hvr-wobble-vertical" onclick="login()">LOGIN</button>

  				 </div>


  				<div class="form-right">

  								<div class="login_social_title">
  									You can login using your social profile.
  								</div>

  								<div class="login_social_list">
  									<ul>
  										<li><a href="#" class="hvr-pulse-shrink"><img src="<?php echo base_url();?>assets/img/icon/social/facebook.ico"></a></li>
  										<li><a href="#" class="hvr-pulse-shrink"><img src="<?php echo base_url();?>assets/img/icon/social/twitter.ico"></a></li>
                      <li><a href="#" class="hvr-pulse-shrink"><img src="<?php echo base_url();?>assets/img/icon/social/google_plus.ico"></a></li>
  										<li><a href="#" class="hvr-pulse-shrink"><img src="<?php echo base_url();?>assets/img/icon/social/instagram.ico"></a></li>

  									</ul>
  								</div>

  								<div class="social_problems">
  									<a href="#">Problem with login?</a>

  									<button type="cancel" class="exit button_exit hvr-wobble-vertical">CANCEL</button>

  								</div>

  				</div>


                              </div>
  					<div class="clearfix"></div>
              <div class="content-signup">
								<?php //$attributes= array('id'=>'register-form','data-toggle'=>'validator'); echo form_open('Auth/signup',$attributes);?>
								<form id="register-form">
                <div class="form-left clearfix">

  						<div class="field_userlogin">
								<?php //echo form_input(array('name'=>'username', 'id'=> 'username', 'placeholder'=>'Username', 'class'=>'form-style', 'value' => set_value('username'),'required'=>'required'	)); ?>
      					<?php //echo form_error('username');?>
								<input type="text" name="username" id="username" class="form-style" placeholder ="Username" />

  							</div>

  							<div class="field_useremail">
									<?php //echo form_input(array('name'=>'email', 'id'=> 'email', 'placeholder'=>'Email Address', 'class'=>'form-style', 'value' => set_value('email'),'required'=>'required','type'=>'email')); ?>
	      					<?php //echo form_error('email');?>
									<input type="email" name="email" id="email" class="form-style" placeholder ="Email Address" required/>
  							</div>

  							<input type="checkbox">
  							<span><span class="span-style"> I agree with </span><a href="#" class="terms"> Terms and Conditions.</a></span>

  							<div class="clearfix"></div>

								 <?php //echo form_submit(array('value'=>'Sign up', 'class'=>'button_login hvr-wobble-vertical')); ?>
								 <button type="button" onclick="signup()" class="button_login hvr-wobble-vertical">Sign Up</button>

  				 		</div>

  				 		<div class="form-right">

  							<div class="field_pass">
									<?php //echo form_input(array('name'=>'password', 'id'=> 'password','type'=>'password', 'placeholder'=>'Password', 'class'=>'form-style', 'value' => set_value('password'),'required'=>'required')); ?>
									<input type="password" name="password" id="password" class="form-style" placeholder ="Password" />
  							</div>

  							<div class="field_pass">
									<?php //echo form_input(array('name'=>'repassword', 'id'=> 'email','type'=>'password', 'placeholder'=>'Email Address', 'class'=>'form-style', 'value' => set_value('repassword'),'required'=>'required')); ?>
									<input type="password" name="repassword" id="repassword" class="form-style" placeholder ="Password" />
  							</div>

  							<span class="span-style-right">Minimum 6 characters</span>
  							<br>
  							<div class="clearfix"></div>
  							<button type="cancel" class="button_exit move hvr-wobble-vertical" id="exit1">CANCEL</button>

  						</div>

            	</div>
  						</form>

  			</div>


  			</div><!-- end of login-header -->
  		</div>
  	</div>

  </div>



<a href="#" class="scroll_to_top hvr-grow" id="scroll"><span class="fa fa-angle-up fa-2x"></span></a>

	<div class="menu_user_wrap animated zoomIn">

			<div class="content_wrap clearfix">

				<div class="menu_user_area menu_user_right menu_user_contact_area">
					<ul>
						<li ><i class="fa fa-user fa-lg"></i><a href="#" id="loginmodal"> Login / Sign-up</a></li>


					</ul>
				</div>

				<div class="menu_user_area menu_user_contact_area menu_user_left">Welcome to Fur And Paws!
				</div>
			</div>




	</div><!-- End of small header -->

	<div class="nav_container">

				<div class="nav_main">
					<div class="nav_left">
						<a href="<?php echo base_url();?>Home"><img src="<?php echo base_url();?>assets/img/furpawlogo.png"></a>
					</div>

					<div class="nav_right">
						<ul>
							<li>
								<a href="<?php echo base_url();?>Home" class="active">Home</a>

								<ul class="sub_nav" id="sub1">
									<li class="li-style"><a href="<?php echo base_url();?>Home/petstore">Pet Store</a></li>
									<li class="li-style"><a href="#">Pet Walking</a></li>
									<li class="li-style"><a href="#">Adoption</a></li>
									<li class="li-style"><a href="#">Club</a></li>
								</ul>
							</li>
							<li>

								<a href="#">Features</a>
								<ul class="sub_nav" id="sub2">
									<li class="li-style"><a href="clinics.html">Pet Clinics</a></li>
									<li class="li-style"><a href="#">Our Services</a></li>
									<li class="li-style"><a href="#">Contact</a></li>
									<li class="li-style"><a href="#">Our Team</a></li>
								</ul>
							</li>
							<li>

								<a href="#">Gallery</a>
								<ul class="sub_nav" id="sub3">
									<li class="li-style"><a href="#">Pets Adoption</a></li>
									<li class="li-style"><a href="#">Pets Sale</a></li>
								</ul>
							</li>
							<li><a href="post-sale.html">Shop</a></li>
							<li><a href="<?php echo base_url();?>Home/adoption"><button class="button green hvr-pulse"><i class="fa fa-check-circle" ></i> ADOPT</button></a></li>
							<li><a href="<?php echo base_url();?>Home/breeder"><button class="button red hvr-pulse"><span class="glyphicon glyphicon-heart"></span> FIND A COUPLE</button></a></li>

						</ul>
					</div>

				</div>
	</div><!-- End of Nav -->

<?php $this->load->view($content_view);?>

	<footer>
		<div class="copyright_wrap">
			<center><p>Fur and Paws &copy; 2016 All Rights Reserved <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a></p></center>
			<center>
				<div class="login_social_list">
					<ul>
							<li><a href="#" class="hvr-wobble-vertical"><img src="<?php echo base_url();?>assets/img/icon/social/facebook.ico" class="icon-circle"></a></li>
							<li><a href="#" class="hvr-wobble-vertical"><img src="<?php echo base_url();?>assets/img/icon/social/twitter.ico" class="icon-circle"></a></li>
							<li><a href="#" class="hvr-wobble-vertical"><img src="<?php echo base_url();?>assets/img/icon/social/google_plus.ico" class="icon-circle"></a></li>
							<li><a href="#" class="hvr-wobble-vertical"><img src="<?php echo base_url();?>assets/img/icon/social/instagram.ico" class="icon-circle"></a></li>

					</ul>
				</div>

		</center>
		</div>
	</footer>


  <!-- Bootstrap And Core JavaScript
    ================================================== -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.barrating.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-elevate.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/loginmodal.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-scroll.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-tooltip.js"></script>


		<script type="text/javascript">

		function signup(){
			var username = $('#username').val();
			var email = $('#email').val();
			var password = $('#password').val();
			var re_password = $('#repassword').val();

			if(username != null && email != null && password !=null && re_password != null){

					if(password == re_password){

						if(password.length>=6){

							var frmdata = $("#register-form").serializeArray(),data={};
							$.each(frmdata, function(i,e){
								if(e.name != 'repassword'){
									data[e.name] = e.value;
								}

							});

							$.post("Auth/signup",{data:data},function(result){
										
							});


						}
						else{
							alert('Password must contains 6 characters');
							$('#password').val('');
							$('#re_password').val('');
							$('#password').focus();
						}

					}
					else{
						alert('Password mismatched try again');
						$('#password').val('');
						$('#re_password').val('');
						$('#password').focus();
					}

			}
			else{
				alert('Fill-up all inputs!');
				$('#username').focus();
			}
			}

function login(){

    var username = $('#login_username').val();
    var password = $('#login_password').val();
		var logindata ={
				'username': username,
				'password':password
		};

    if(username != null && password !=null){

			var frmdata = $("#login-form").serializeArray(),data={};
			$.each(frmdata, function(i,e){
					data[e.name] = e.value;

			});

			$.post("Auth/login",{data:data},function(result){
                            if(result ==0){
                                alert(result);
                            }
                            else if(result != true){
                                window.location.href = result;
                            }
                        });

    }else{

      alert('Please fill all inputs..');
      $('#username').val('');
      $('#password').val('');
      $('#username').focus();
    }
}
		</script>

</body>
</html>
