<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/mainstyle.css">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/furpawlogo.png">
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/thumbnail.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
    <link href="<?php echo base_url();?>assets/css/hover-min.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
      <script>
        $(document).ready(function(){

            $("#getphotos").on('change', function () {

                if (typeof (FileReader) != "undefined") {

                    var image_holder = $("#image-holder");
                    image_holder.empty();

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("<img />", {
                            "src": e.target.result,
                            "class": "thumb-image",
                            "width":"200",
                            "height":"100"
                        }).appendTo(image_holder);

                    }
                    image_holder.show();
                    reader.readAsDataURL($(this)[0].files[0]);
                } else {
                    alert("This browser does not support FileReader.");
                }
            });

        });
        
        function logout(){
        
        $.post("<?php echo base_url();?>Auth/logout",function(result){
            window.location.href= result;
        });
        }
        
        function updateSettings(){
             
                var mypass  = $('#currentpassword').val();
                var email = $('#email').val();
                var newpass = $('#newpass').val();
                var repass = $('#repass').val();
                

                if(newpass!="" && repass!=""){

                        if(newpass == repass){
                            var data = {
                               'newpass':newpass,
                               'email':email
                            };
                            
                            $.post("<?php echo base_url();?>Profile/UProfile/updateSettings",{data:data},function(result){
                                alert($.trim(result));
                            });
                        }
                        else{
                          alert('Password mismatched');
                          $('#newpass').val('');
                          $('#repass').val('');
                          $('#newpass').focus();
                        }

                   
                }

                else{
                    
                    var items = {'email':email};
                    $.post("<?php echo base_url();?>Profile/UProfile/updateSettingsNp",{items:items},function(result){
                        alert(result);
                    });
                }
        }
        
        function deletePhoto(){
            var items = $('#photousername').val();
            
            if(confirm('Are you sure you want to delete your Profile Photo ?')){
                
                $.post("<?php echo base_url();?>Profile/IProfile/deleteprofilePhoto",{items:items},function(result){
                    
                    window.location.href = result;
                });
            }
            
        }
        
        function deleteCover(){
            var items = $('#photousername').val();
            
            if(confirm('Are you sure you want to delete your cover photo ?')){
                
                $.post("<?php echo base_url();?>Profile/IProfile/deleteprofileCover",{items:items},function(result){
                    
                    window.location.href = result;
                });
            }
            
        }
 
    </script>
    <title>FurAndPaws-<?php echo $title;?></title>

</head>

<body>
    
    <input type="hidden" id="photousername" value="<?php echo $id; ?>"/>   
<!-- Modals	 -->

<!--Profile Modal -->
<div class="profilemodal" hidden>

    <div class="profile-content">

        <div class="profile-header">
            <button type="button" id="logout-button" class="close pull-right" data-dismiss="modal" aria-label="Close" style="position:absolute;top: 10px;right:25px;"><span aria-hidden="true" style="font-size:30px">&times;</span></button>

            <div class="profile-main-content">

                <div class="user-account">
                    <div class="user_account">
                        <span class="user_avatar"><img src="<?php echo base_url('assets/img/uploaded_image/'.$profilepicture);?>"></span>
                        <span class="user_text">My account:</span>
                        <span class="user_name"><?php echo ucfirst($username); ?></span>
                    </div>

                    <div class="user_cart">
                        <span class="cart_text">Welcome! <?php echo ucfirst($username); ?></span>
                    </div>

                    <div class="user_info">
                        <div class="settings">
                            <span class="fa fa-cogs fa-3x blue"></span><a href="<?php echo base_url();?>Profile/profileSettings">Settings</a>
                        </div>
                        <div class="help">
                            <span class="fa fa-life-bouy fa-3x blue"></span><a href="#"> Help</a>
                        </div>
                    </div>

                    <div class="user_logout">
                        <button class="button-search" onclick="logout()"style="width:100%">LOG OUT</button>
                    </div>
                </div>

            </div>


        </div><!-- end of login-header -->
    </div>
</div>


<a href="#" class="scroll_to_top hvr-grow" id="scroll"><span class="fa fa-angle-up fa-2x"></span></a>

<div class="menu_user_wrap">

    <div class="content_wrap clearfix">

        <div class="menu_user_area menu_user_right menu_user_contact_area">
            <ul>
                <li ><i class="fa fa-user fa-lg"></i><a href="#" id="profile-modal"> My Account</a></li>


            </ul>
        </div>

        <div class="menu_user_area menu_user_contact_area menu_user_left">Welcome to Fur And Paws!
        </div>
    </div>




</div><!-- End of small header -->






<div class="nav_container">

				<div class="nav_main">
					<div class="nav_left">
						<a href="<?php echo base_url();?>Profile"><img src="<?php echo base_url();?>assets/img/furpawlogo.png"></a>
					</div>

					<div class="nav_right">
						<ul>
							<li>
								<a href="<?php echo base_url();?>Profile" class="active">Home</a>

								<ul class="sub_nav" id="sub1">
									<li class="li-style"><a href="<?php echo base_url();?>Profile/petstore">Pet Store</a></li>
									<li class="li-style"><a href="#">Pet Walking</a></li>
									<li class="li-style"><a href="<?php echo base_url();?>Profile/adoption">Adoption</a></li>
									<li class="li-style"><a href="#">Club</a></li>
								</ul>
							</li>
							<li>

								<a href="#">Features</a>
								<ul class="sub_nav" id="sub2">
									<li class="li-style"><a href="<?php echo base_url();?>Profile/clinics">Pet Clinics</a></li>
									<li class="li-style"><a href="#">Our Services</a></li>
									<li class="li-style"><a href="#">Contact</a></li>
									<li class="li-style"><a href="#">Our Team</a></li>
								</ul>
							</li>
							<li>

								<a href="#">Gallery</a>
								<ul class="sub_nav" id="sub3">
									<li class="li-style"><a href="<?php echo base_url();?>Profile/adoption">Pets Adoption</a></li>
									<li class="li-style"><a href="<?php echo base_url();?>Profile/petstore">Pets Sale</a></li>
								</ul>
							</li>
							<li><a href="<?php echo base_url();?>Profile/postsale">Shop</a></li>
							<li><a href="<?php echo base_url();?>Profile/adoption"><button class="button green hvr-pulse"><i class="fa fa-check-circle" ></i> ADOPT</button></a></li>
							<li><a href="<?php echo base_url();?>Profile/breeder"><button class="button red hvr-pulse"><span class="glyphicon glyphicon-heart"></span> FIND A BREEDER</button></a></li>

						</ul>
					</div>

				</div>
	</div><!-- End of Nav -->

<?php $this->load->view($content_view);?>


<footer class="clearfix">
    <div class="copyright_wrap">
        <center><p>Fur and Paws &copy; 2016 All Rights Reserved <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a></p></center>
        <center><div class="login_social_list">
            <ul>
                <li><a href="#" class="hvr-wobble-vertical"><img src="<?php echo base_url();?>assets/img/icon/social/facebook.ico" class="icon-circle"></a></li>
                <li><a href="#" class="hvr-wobble-vertical"><img src="<?php echo base_url();?>assets/img/icon/social/twitter.ico" class="icon-circle"></a></li>
                <li><a href="#" class="hvr-wobble-vertical"><img src="<?php echo base_url();?>assets/img/icon/social/google_plus.ico" class="icon-circle"></a></li>
                <li><a href="#" class="hvr-wobble-vertical"><img src="<?php echo base_url();?>assets/img/icon/social/instagram.ico" class="icon-circle"></a></li>

            </ul>
        </div>
        </center>
    </div>
</footer><!-- end of footer -->


<!-- Bootstrap And Core JavaScript
  ================================================== -->

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/profilemodal.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/subscriptionmodal.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-scroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom-tooltip.js"></script>

</body>
</html>
