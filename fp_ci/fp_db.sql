-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 23, 2016 at 09:43 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fp_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `adoptions`
--

CREATE TABLE IF NOT EXISTS `adoptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `old_owner_id` int(11) NOT NULL,
  `new_owner_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `breeds`
--

CREATE TABLE IF NOT EXISTS `breeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `description` varchar(255) NOT NULL,
  `pets_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pets_id` (`pets_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `ownable_type` varchar(50) NOT NULL,
  `ownable_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `enthusiast`
--

CREATE TABLE IF NOT EXISTS `enthusiast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact_number` varchar(11) NOT NULL,
  `updated_at` timestamp NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `enthusiast`
--

INSERT INTO `enthusiast` (`id`, `name`, `gender`, `address`, `contact_number`, `updated_at`, `user_id`) VALUES
(2, 'John', 'Female', 'Mambaling', '09192964425', '2016-08-22 01:31:55', 52);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img_name` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `imageable_type` varchar(20) NOT NULL,
  `imageable_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `img_name`, `image`, `created_at`, `updated_at`, `imageable_type`, `imageable_id`) VALUES
(17, 'Tulips4', 'Tulips4.jpg', '2016-08-21 02:30:43', '2016-08-21 18:48:52', 'Profile_cover', 51),
(22, 'Desert4', 'Desert4.jpg', '2016-08-21 03:44:42', '2016-08-21 17:14:02', 'Profile_photo', 58),
(24, 'Tulips3', 'Tulips3.jpg', '2016-08-21 17:14:13', '2016-08-21 17:14:13', 'Profile_cover', 58),
(25, 'Desert5', 'Desert5.jpg', '2016-08-21 18:50:33', '2016-08-21 18:50:33', 'Profile_photo', 59),
(26, 'Chrysanthemum1', 'Chrysanthemum1.jpg', '2016-08-21 19:24:23', '2016-08-21 19:24:23', 'Profile_photo', 51),
(37, 'john1', 'john1.jpg', '2016-08-22 18:59:19', '2016-08-22 18:59:19', 'Pet_photo', 9),
(38, 'zetta1', 'zetta1.jpg', '2016-08-22 18:59:41', '2016-08-22 18:59:41', 'Pet_photo', 10),
(39, 'karen', 'karen.jpg', '2016-08-22 19:00:03', '2016-08-22 19:00:03', 'Pet_photo', 11);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `price` int(11) NOT NULL,
  `description` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pets`
--

CREATE TABLE IF NOT EXISTS `pets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `breed` varchar(50) NOT NULL,
  `age` varchar(30) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `color` varchar(50) NOT NULL,
  `description` varchar(300) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `ownable_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `pets`
--

INSERT INTO `pets` (`id`, `name`, `breed`, `age`, `gender`, `color`, `description`, `created_at`, `updated_at`, `ownable_id`) VALUES
(9, 'testing', 'testing', 'testing', 'Female', 'asdasd', 'asdqweasdsadasd', '2016-08-22 18:59:19', '2016-08-22 18:59:19', 52),
(10, 'next pets', 'qweasd', '213asdasdsa', 'Female', 'qweasdsad', 'qweasdsa', '2016-08-22 18:59:41', '2016-08-22 18:59:41', 52),
(11, 'anotherpets', 'qweasd', '24', 'Female', 'asdasd', 'qeasdasdas', '2016-08-22 19:00:03', '2016-08-22 19:00:03', 52);

-- --------------------------------------------------------

--
-- Table structure for table `petshops`
--

CREATE TABLE IF NOT EXISTS `petshops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact_number` varchar(11) NOT NULL,
  `updated_at` timestamp NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `petshops`
--

INSERT INTO `petshops` (`id`, `name`, `gender`, `address`, `contact_number`, `updated_at`, `user_id`) VALUES
(2, 'John', 'Female', 'Mambaling', '09192964425', '2016-08-22 01:31:55', 52);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `user_id` int(11) NOT NULL,
  `breed_id` int(11) NOT NULL,
  `pet_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `breed_id` (`breed_id`),
  KEY `pet_id` (`pet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `ownable_type` int(11) NOT NULL,
  `ownable_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_id` (`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `ownable_type` varchar(50) NOT NULL,
  `ownable_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `user_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `created_at`, `updated_at`, `user_type`) VALUES
(51, 'admin1234@yahoo.com', 'admin', 'a8f5f167f44f4964e6c998dee827110c', '2016-08-19 17:44:52', '2016-08-21 19:25:52', 'Pet Enthusiast'),
(52, 'admin1234@yahoo.com', 'admin', 'f6fdffe48c908deb0f4c3bd36c032e72', '2016-08-19 17:45:28', '2016-08-21 22:57:19', 'Pet Enthusiast'),
(53, 'johnberdida@yahoo.com', 'johnthel10', 'f6fdffe48c908deb0f4c3bd36c032e72', '2016-08-19 17:48:10', '2016-08-19 17:48:10', ''),
(54, 'asdsad@yasdsad.com', 'asdsad', 'f6fdffe48c908deb0f4c3bd36c032e72', '2016-08-19 19:19:08', '2016-08-19 19:19:08', ''),
(55, 'johnberdida@yahoo.com', 'asdsa', 'a8f5f167f44f4964e6c998dee827110c', '2016-08-19 19:28:27', '2016-08-19 19:28:27', ''),
(56, 'asdasda@yahoo.com', 'asdsa', 'a8f5f167f44f4964e6c998dee827110c', '2016-08-19 19:44:44', '2016-08-19 19:44:44', ''),
(57, 'jLkisni@yahoo.com', 'jLkisni', '3f6e0d3aeb6c403803a8ddf4c93678a7', '2016-08-19 18:08:17', '2016-08-19 18:08:17', ''),
(58, 'nino@yahoo.com', 'nino', 'a8f5f167f44f4964e6c998dee827110c', '2016-08-19 22:11:41', '2016-08-20 21:17:13', 'Pet Enthusiast'),
(59, 'john@yahoo.com', 'john', 'f6fdffe48c908deb0f4c3bd36c032e72', '2016-08-21 18:49:57', '2016-08-21 18:50:14', 'Pet Enthusiast');

-- --------------------------------------------------------

--
-- Table structure for table `veterinary_clinics`
--

CREATE TABLE IF NOT EXISTS `veterinary_clinics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact_number` varchar(11) NOT NULL,
  `updated_at` timestamp NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `veterinary_clinics`
--

INSERT INTO `veterinary_clinics` (`id`, `name`, `gender`, `address`, `contact_number`, `updated_at`, `user_id`) VALUES
(2, 'John', 'Female', 'Mambaling', '09192964425', '2016-08-22 01:31:55', 52);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `breeds`
--
ALTER TABLE `breeds`
  ADD CONSTRAINT `breeds_ibfk_1` FOREIGN KEY (`pets_id`) REFERENCES `pets` (`id`);

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `post_ibfk_2` FOREIGN KEY (`breed_id`) REFERENCES `breeds` (`id`),
  ADD CONSTRAINT `post_ibfk_3` FOREIGN KEY (`pet_id`) REFERENCES `pets` (`id`);

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
